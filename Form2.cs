﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using ZedGraph;

namespace Cycle
{
    public partial class Form2 : Form
    {
        delegate void SetTextCallback(string text);
        delegate void axisChangeZedGraphCallBack(ZedGraphControl zg);
        public Thread garthererThread;
        private System.Timers.Timer gartherTimer;
        LineItem myCurve, myCurve2, myCurve3, myCurve4, myCurve5;
        PointPairList list1;
        PointPairList list2;
        PointPairList list3;
        PointPairList list4;
        PointPairList list5;
        GraphPane myPane;





        public Form2()
        {
            InitializeComponent();
        }
        string[] ARRHR;
        string[] ARRSPD;
        string[] ARRCAD;
        string[] ARRALT;
        string[] ARRPWR;
        



        private void axisChangeZedGraph(ZedGraphControl zg)
        {
            if (zg.InvokeRequired)
            {
                axisChangeZedGraphCallBack ad = new axisChangeZedGraphCallBack(axisChangeZedGraph);
                zg.Invoke(ad, new object[] { zg });
            }
            else
            {
                zg1.AxisChange();
                zg.Invalidate();
                zg.Refresh();
            }
        }


        public void Thread1()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            ARRHR = new string[arrData.Length - index2];
            ARRSPD = new string[arrData.Length - index2];
            ARRCAD = new string[arrData.Length - index2];
            ARRALT = new string[arrData.Length - index2];
            ARRPWR = new string[arrData.Length - index2];
            int j = 0;



            for (int i = index2 + 1; i < arrData.Length; i++)
            {
                string HRData = arrData[i];

                string[] arrHrdata = Regex.Split(HRData, @"\W+");

                ARRHR[j] = arrHrdata[0];

                this.SetText(ARRHR[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }

        public void Thread2()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            ARRHR = new string[arrData.Length - index2];
            ARRSPD = new string[arrData.Length - index2];
            ARRCAD = new string[arrData.Length - index2];
            ARRALT = new string[arrData.Length - index2];
            ARRPWR = new string[arrData.Length - index2];
            int j = 0;



            for (int i = index2 + 1; i < arrData.Length - 1; i++)
            {
                string HRData = arrData[i];

                string[] arrHrdata = Regex.Split(HRData, @"\W+");

                ARRSPD[j] = arrHrdata[1];

                this.SetText1(ARRSPD[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }


        public void Thread3()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            ARRHR = new string[arrData.Length - index2];
            ARRSPD = new string[arrData.Length - index2];
            ARRCAD = new string[arrData.Length - index2];
            ARRALT = new string[arrData.Length - index2];
            ARRPWR = new string[arrData.Length - index2];
            int j = 0;

            for (int i = index2 + 1; i < arrData.Length - 1; i++)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                ARRCAD[j] = arrHrdata[2];

                this.SetText2(ARRCAD[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }


        public void Thread4()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            ARRHR = new string[arrData.Length - index2];
            ARRSPD = new string[arrData.Length - index2];
            ARRCAD = new string[arrData.Length - index2];
            ARRALT = new string[arrData.Length - index2];
            ARRPWR = new string[arrData.Length - index2];
            int j = 0;

            for (int i = index2 + 1; i < arrData.Length - 1; i++)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");

                ARRALT[j] = arrHrdata[3];

                this.SetText3(ARRALT[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }


        public void Thread5()
        {
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            ARRHR = new string[arrData.Length - index2];
            ARRSPD = new string[arrData.Length - index2];
            ARRCAD = new string[arrData.Length - index2];
            ARRALT = new string[arrData.Length - index2];
            ARRPWR = new string[arrData.Length - index2];
            int j = 0;

            for (int i = index2 + 1; i < arrData.Length - 1; i++)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                ARRPWR[j] = arrHrdata[4];

                this.SetText4(ARRPWR[j].ToString());
                Thread.Sleep(700);
                j++;
            }
        }



        private void TimerCallback(object source, ElapsedEventArgs e)
        {
            garthererThread = new Thread(new ThreadStart(GartherData));
        }

        private void GartherData()
        {
            List<float> gartheredInfo = new List<float>();

            //Do your garthering and parsing here (and put it in the gartheredInfo variable)

            //InformationHolder.Instance().graphData = gartheredInfo;

            zg1.Invoke(new MethodInvoker( //you need to have a reference to the form
                    delegate
                    {
                        zg1.Invalidate(); //or another method that redraws the graph
                    }));
        }

        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.

            try
            {
                if (this.label6.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(SetText);
                    this.Invoke(d, new object[] { text });
                }
                else
                {
                    this.label6.Text = text;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

       

        private void Form2_Load_1(object sender, EventArgs e)
        {
            // Generate a red curve with diamond
            // symbols, and "Porsche" in the legend
            myPane = zg1.GraphPane;
            list1 = new PointPairList();
            list2 = new PointPairList();
            list3 = new PointPairList();
            list4 = new PointPairList();
            list5 = new PointPairList();
            myCurve = myPane.AddCurve("HR",
                 list1, Color.Red, SymbolType.Diamond);

            // Generate a blue curve with circle
            // symbols, and "Piper" in the legend
            myCurve2 = myPane.AddCurve("Speed",
                 list2, Color.Blue, SymbolType.Circle);


            myCurve3 = myPane.AddCurve("CAD",
               list3, Color.Green, SymbolType.Diamond);


            myCurve4 = myPane.AddCurve("ALT",
                 list4, Color.Purple, SymbolType.Diamond);


            myCurve5 = myPane.AddCurve("PWR",
                 list5, Color.Brown, SymbolType.Diamond);

            Thread tid1 = new Thread(new ThreadStart(Thread1));
            Thread tid2 = new Thread(new ThreadStart(Thread2));
            Thread tid3 = new Thread(new ThreadStart(Thread3));
            Thread tid4 = new Thread(new ThreadStart(Thread4));
            Thread tid5 = new Thread(new ThreadStart(Thread5));
            Thread tidgc = new Thread(new ThreadStart(CreateGraph));
            tid1.Start();
            tid2.Start();
            tid3.Start();
            tid4.Start();
            tid5.Start();
            tidgc.Start();

        }

        private void SetText1(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.label7.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText1);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.label7.Text = text;
            }
        }

        private void headerInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 a = new Form1();
            a.ShowDialog();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

        private void dataGredViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 fa3 = new Form3();
            fa3.ShowDialog();
        }

        private void SetText2(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.label8.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText2);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.label8.Text = text;
            }
        }

        private void SetText3(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.label9.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText3);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.label9.Text = text;
            }
        }

        private void SetText4(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.label10.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText4);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.label10.Text = text;
            }
        }


        



        private void CreateGraph()
        {
            // get a reference to the GraphPane


            // Set the Titles
            myPane.Title.Text = "My Graph";
            myPane.XAxis.Title.Text = "lINE NUM";
            myPane.YAxis.Title.Text = "HR, SPD, CAD, ALT, PWR";



            ///
            string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            ARRHR = new string[arrData.Length - index2];
            ARRSPD = new string[arrData.Length - index2];
            ARRCAD = new string[arrData.Length - index2];
            ARRALT = new string[arrData.Length - index2];
            ARRPWR = new string[arrData.Length - index2];
            int j = 0;


            // Make up some, data arrays based on the Sine function
            double x, y1, y2, y3, y4, y5;
            PointPairList list1 = new PointPairList();
            PointPairList list2 = new PointPairList();
            PointPairList list3 = new PointPairList();
            PointPairList list4 = new PointPairList();
            PointPairList list5 = new PointPairList();

            for (int i = index2 + 1; i < arrData.Length - 1; i += 1)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");

                ARRHR[j] = arrHrdata[0];
                ARRSPD[j] = arrHrdata[1];
                ARRCAD[j] = arrHrdata[2];
                ARRALT[j] = arrHrdata[3];
                ARRPWR[j] = arrHrdata[4];

                int a = Int32.Parse(ARRHR[j]);
                int b = Int32.Parse(ARRSPD[j]);
                int c = Int32.Parse(ARRCAD[j]);
                int d = Int32.Parse(ARRALT[j]);
                int e = Int32.Parse(ARRPWR[j]);
                x = i;
                y1 = a;
                y2 = b;
                y3 = c;
                y4 = d;
                y5 = e;

                
                LineItem curve = zg1.GraphPane.CurveList["HR"] as LineItem;
                // Get the PointPairList
                IPointListEdit list = curve.Points as IPointListEdit;
                list.Add(x, y1);
                LineItem curve2 = zg1.GraphPane.CurveList["Speed"] as LineItem;
                // Get the PointPairList
                IPointListEdit list12 = curve2.Points as IPointListEdit;
                list12.Add(x, y2);
                LineItem curve3 = zg1.GraphPane.CurveList["CAD"] as LineItem;
                // Get the PointPairList
                IPointListEdit list13 = curve3.Points as IPointListEdit;
                list13.Add(x, y3);
                LineItem curve4 = zg1.GraphPane.CurveList["ALT"] as LineItem;
                // Get the PointPairList
                IPointListEdit list14 = curve4.Points as IPointListEdit;
                list14.Add(x, y4);
                LineItem curve5 = zg1.GraphPane.CurveList["PWR"] as LineItem;
                // Get the PointPairList
                IPointListEdit list15 = curve5.Points as IPointListEdit;
                list15.Add(x, y5);
                

                axisChangeZedGraph(zg1);
                Thread.Sleep(700);

            }

        }

        private void advanceMetricsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Advancemetric ad = new Advancemetric();
            ad.ShowDialog();
        }

        private void intervalDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            interval ins = new interval();
            ins.ShowDialog();
        }

        private void zg1_Load(object sender, EventArgs e)
        {

        }


    }


}
