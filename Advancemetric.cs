﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Cycle
{
    public partial class Advancemetric : Form
    {
        public Advancemetric()
        {
            InitializeComponent();
        }


          int[] LXPWR;
            int[] ALXPWR;
            int[] THAPAPWR;
            double avgpwr = 0;
            double avgthapapwr = 0;
            double avglxpwr = 0;
            

        private void Advancemetric_Load(object sender, EventArgs e)
        {
               //----------------for ASDBExampleCycleComputerData.hrm data File to read--------------
            try
            {
                string txtData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");
            
                ALXPWR = new int[arrData.Length - (index2 + 1)];


                int j = 0;
                double powertotal = 0;
                
                for (int i = index2 + 1; i < arrData.Length; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");
                    ALXPWR[j] = Convert.ToInt32(arrHrdata[4]);

                    powertotal = powertotal + ALXPWR[j];
                    j++;
                }

                //for average power
                avgpwr = Math.Round(powertotal / (j));
                this.label12.Text = avgpwr.ToString("#.###") + " watts";
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }





            //----------------for data1.hrm data File to read--------------
            try
            {
                string txtData = File.ReadAllText("data1.hrm");
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int indexlx = Array.IndexOf(arrData, "[HRData]");

                LXPWR = new int[arrData.Length - (indexlx + 1)];


                int lx = 0;
                double lxpowertotal = 0;

                for (int i = indexlx + 1; i < arrData.Length; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");
                    LXPWR[lx] = Convert.ToInt32(arrHrdata[4]);

                    //Average power
                    lxpowertotal = lxpowertotal + LXPWR[lx];
                    lx++;
                }

                //for average power
                avglxpwr = Math.Round(lxpowertotal / (lx));
                
                this.label13.Text = avglxpwr.ToString("#.###") + " watts";
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

         

            //----------------for data2.hrm data File to read--------------
            try
            {
                string txtData = File.ReadAllText("data2.hrm");
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int indexthapa = Array.IndexOf(arrData, "[HRData]");

                THAPAPWR = new int[arrData.Length - (indexthapa + 1)];


                int thapa = 0;
                double thapapowertotal = 0;

                for (int i = indexthapa + 1; i < arrData.Length; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");
                    THAPAPWR[thapa] = Convert.ToInt32(arrHrdata[4]);

                    //Average power
                    thapapowertotal = thapapowertotal + THAPAPWR[thapa];
                    thapa++;
                }

                //for average power
                 avgthapapwr = Math.Round(thapapowertotal / (thapa));
                this.label16.Text = avgthapapwr.ToString("#.###") + " watts";
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            // calculating Functional Threshold Power (FTP): 
            double ftp = avgpwr * 0.05;
            double totalftp = avgpwr - ftp;
            this.label20.Text = totalftp.ToString("#.#####") + " watts";


            //Calculating Normalized Power (NP)

            double f1 = Math.Pow(avgpwr, 4.0);
            double f2 = Math.Pow(avglxpwr, 4.0);
            double f3 = Math.Pow(avgthapapwr, 4.0);

            double m1 = f1 * 66.0;
            double m2 = f2 * 10.0;
            double m3 = f3 * 27.0;

            double totalm = m1 + m2 + m3;
            double timedoubel = totalm / 60.0;
            double np = Math.Pow(timedoubel, (1.0 / 4));

            this.label21.Text = np.ToString("#.###") + " watts";


            //calculating Intensity Factor® (IF®):

            double iF = np / totalftp;
            this.label24.Text = iF.ToString("#.###");



            //calculating tss

            //TSS = (sec x NP® x IF®)/(FTP x 3600) x 100

            double tss = ((3960 * np * iF) / (totalftp * 3600)) * 100;


            label26.Text = tss.ToString("#.###");


        }

        private void Advancemetric_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void advanceMetricsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 ab3 = new Form3();
            ab3.ShowDialog();
        }

        private void headerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 a = new Form1();
            a.ShowDialog();
        }

        private void graphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 fff2 = new Form2();
            fff2.ShowDialog();
        }

        private void intervalDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            interval ins = new interval();
            ins.ShowDialog();
        }



        }
    }

