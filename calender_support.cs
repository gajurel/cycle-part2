﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cycle
{
    public partial class calender_support : Form
    {
        public calender_support()
        {
            InitializeComponent();
        }

        private void calender_support_Load(object sender, EventArgs e)
        {
            int[] ARRHR;
            int[] ARRSPD;
            int[] ARRCAD;
            int[] ARRALT;
            int[] ARRPWR;


            string file1 = label9.Text;
          //  string file2 = apple9.Text;

            //Header Information
            foreach (string line in File.ReadAllLines(file1))
            {
                string[] parts = line.Split('=');


                if (parts[0].Equals("Version"))
                {
                    label21.Text = parts[1];
                }

                if (parts[0].Equals("Monitor"))
                {
                    label22.Text = parts[1];
                }

                if (parts[0].Equals("StartTime"))
                {
                    label23.Text = parts[1];
                }

                if (parts[0].Equals("Length"))
                {
                    label24.Text = parts[1];
                }

                if (parts[0].Equals("SMode"))
                {
                    label13.Text = parts[1];
                }

                if (parts[0].Equals("Date"))
                {
                    label25.Text = parts[1];
                }

                if (parts[0].Equals("Interval"))
                {
                    label26.Text = parts[1];
                }

               

            }

            // Datagrid view column header
        
            dataGridView1.Columns.Add("HR", "HR");
            dataGridView1.Columns.Add("SPD", "SPD");
            dataGridView1.Columns.Add("CAD", "CAD");
            dataGridView1.Columns.Add("ALT", "ALT");
            dataGridView1.Columns.Add("PWR", "PWR");

            string txtData = File.ReadAllText(file1);
            string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int index2 = Array.IndexOf(arrData, "[HRData]");
            ARRHR = new int[arrData.Length - (index2 + 1)];
            ARRSPD = new int[arrData.Length - (index2 + 1)];
            ARRCAD = new int[arrData.Length - (index2 + 1)];
            ARRALT = new int[arrData.Length - (index2 + 1)];
            ARRPWR = new int[arrData.Length - (index2 + 1)];


            int j = 0;
            double total = 0;
            double heart = 0;
            double powertotal = 0;
            double alt = 0;


            for (int i = index2 + 1; i < arrData.Length; i++)
            {
                string HRData = arrData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");

                ARRHR[j] = Convert.ToInt32(arrHrdata[0]);
                ARRSPD[j] = Convert.ToInt32(arrHrdata[1]);
                ARRCAD[j] = Convert.ToInt32(arrHrdata[2]);
                ARRALT[j] = Convert.ToInt32(arrHrdata[3]);
                ARRPWR[j] = Convert.ToInt32(arrHrdata[4]);


                // for Average speed
                double speed = Convert.ToDouble(arrHrdata[1]) / 10;
                double xa = ARRSPD[j];
                total = total + xa;

                //Average heart Rate
                double hrt = ARRHR[j];
                heart = heart + hrt;


                //Average power
                double pwr = ARRPWR[j];
                powertotal = powertotal + pwr;

                double altitude = ARRALT[j];
                alt = alt + altitude;

                // Display data in DataGrid view
                dataGridView1.Rows.Add(new object[] { arrHrdata[0], speed.ToString(), ARRCAD[j], ARRALT[j], ARRPWR[j], (j + 1).ToString() });
                j++;
            }




            double km = total / 10;

            //FOr average speed
            double avgspeed = km / (j);
            this.label16.Text = avgspeed.ToString("#.####");


            //for average Heart Rate
            double avghr = heart / (j);
            this.label12.Text = avghr.ToString("#.###");
           

            double avgpwr = powertotal / (j);
            this.label18.Text = avgpwr.ToString("#.###");

            //for average ALT data
            double avgalt = alt / (j);
            this.label15.Text = avgalt.ToString("#.###");



          
        }

        }
    }

